Description: fixes #1028471 by fixing build on gcc 11
We do this by applying https://gitlab.common-lisp.net/cmucl/cmucl/-/merge_requests/84 to the source
Author: Peter Van Eynde <pevaneyn@debian.org>
Origin: upstream
Bug: https://gitlab.common-lisp.net/cmucl/cmucl/-/issues/122
Applied-Upstream: https://gitlab.common-lisp.net/cmucl/cmucl/-/merge_requests/84
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- cmucl.orig/src/lisp/Config.x86_linux
+++ cmucl/src/lisp/Config.x86_linux
@@ -1,7 +1,7 @@
 # -*- Mode: makefile -*-
 include Config.x86_common
 
-CPPFLAGS += -m32 -D__NO_CTYPE -D_GNU_SOURCE
+CPPFLAGS += -m32 -D__NO_CTYPE
 CPPFLAGS += $(shell dpkg-buildflags --get CPPFLAGS)
 CFLAGS += -rdynamic  -march=pentium4 -mfpmath=sse -mtune=generic
 CFLAGS += $(shell dpkg-buildflags --get CFLAGS)
--- cmucl.orig/src/lisp/Config.x86_linux_clang
+++ cmucl/src/lisp/Config.x86_linux_clang
@@ -2,7 +2,7 @@
 include Config.x86_common
 
 CC = clang
-CPPFLAGS += -m32 -D__NO_CTYPE -D_GNU_SOURCE
+CPPFLAGS += -m32 -D__NO_CTYPE
 CPPFLAGS += $(shell dpkg-buildflags --get CPPFLAGS)
 CFLAGS += -march=pentium4 -mfpmath=sse -mtune=generic
 CFLAGS += $(shell dpkg-buildflags --get CFLAGS)
--- cmucl.orig/src/lisp/Linux-os.c
+++ cmucl/src/lisp/Linux-os.c
@@ -17,6 +17,8 @@
  *
  */
 
+#define _GNU_SOURCE /* for reg_* constants in uc_mcontext.gregs  */
+#include <signal.h>
 #include <stdio.h>
 #include <sys/param.h>
 #include <sys/file.h>
@@ -198,6 +200,8 @@
 	return (unsigned long *) &scp->uc_mcontext.gregs[REG_ESI];
     case 14:
 	return (unsigned long *) &scp->uc_mcontext.gregs[REG_EDI];
+    case 16:
+	return (unsigned long*) &scp->uc_mcontext.gregs[REG_EFL];
     }
     return NULL;
 }
--- cmucl.orig/src/lisp/x86-lispregs.h
+++ cmucl/src/lisp/x86-lispregs.h
@@ -30,6 +30,7 @@
 #define reg_EBP REG(10)
 #define reg_ESI REG(12)
 #define reg_EDI REG(14)
+#define reg_EFL REG(16)
 
 #define reg_SP reg_ESP
 #define reg_FP reg_EBP
@@ -60,7 +61,7 @@
 #define SC_EFLAGS(sc) ((sc)->uc_mcontext->ss.eflags)
 #endif
 #elif defined(__linux__)
-#define SC_EFLAGS(sc) ((sc)->uc_mcontext.gregs[REG_EFL])
+#define SC_EFLAGS(sc) SC_REG(sc, reg_EFL)
 #elif defined(__NetBSD__)
 #define SC_EFLAGS(sc) ((sc)->uc_mcontext.__gregs[_REG_EFL])
 #elif defined(SOLARIS)
