;;; -*- Mode: Lisp; Package: System -*-
;;;
;;; **********************************************************************
;;; This code was written as part of the CMU Common Lisp project at
;;; Carnegie Mellon University, and has been placed in the public domain.
;;;

;;; Heavy modifications by Peter Van Eynde

(in-package "SYSTEM")

#+(or)
(if (probe-file "/etc/lisp-config.lisp")
    (load "/etc/lisp-config.lisp")
  (format t "~%;;; Hey: there is no /etc/lisp-config.lisp file, please run \"dpkg-reconfigure common-lisp-controller\" as root"))

;;; If you have sources installed on your system, un-comment the following form
;;; and change it to point to the source location.  This will allow the Hemlock
;;; "Edit Definition" command and the debugger to find sources for functions in
;;; the core.
(setf (ext:search-list "target:")
      '(
        "/usr/share/common-lisp/source/cmucl/"      ; object dir
        ))

(setf (ext:search-list "library:") '("/usr/lib/cmucl/"))
;;; for safety...

;;; optional extentions to the lisp image: delete if you
;;; don't like them :-).
(in-package :common-lisp-user)

;;; newbie functions, delete if you don't like them
(defun help ()
  (format t "~
Welcome to CMUCL for Linux.

If you aren't running this with ilisp in emacs,
or aren't intending to use hemlock then you
deserve to lose. :-)

Read the documentation in /usr/share/doc/cmucl.

(quit) is to quit.
(ed) starts hemlock (if installed)
(demo) shows a list of demos
(describe 'progn) gives information about progn for 
 example.
(inspect '*a*) interactively inspects *a* for example.
"))

(defun demo ()
  (format t "Some demos are in the source package, some in the
normal package.

General demos:
CLX demos:
 (require :clx)
 (load \"/usr/share/common-lisp/source/cmucl-clx/demo/hello.lisp\")
 
 (xlib::hello-world \"\")
 (load \"/usr/share/common-lisp/source/cmucl-clx/demo/menu.lisp\")
 (xlib::just-say-lisp \"\")
 (xlib::pop-up \"\"
	       '(\"Linux\" \"FreeBSD\" \"OpenBSD\"))
  exit by pressing control+C followed by a keypress.

Clue demos:
 if you have installed the clue package you can do:
 (clc:clc-require :clue)
 (load \"/usr/share/common-lisp/source/clue/examples/menu\")
 (clue-examples::beatlemenuia \"\")
 (clue-examples::pick-one \"\"
			  \"One\"
			  \"Two\"
			  \"Three\")
 (clue-examples::just-say-lisp \"\")
 
 or you can use the Clio demos:
 (clc:clc-require :clio)
 (clc:clc-require :clio-examples)
 (clio-examples::sketch :host  \"\")

Pictures demos:
 (clc:clc-require :pictures)
 (load \"/usr/share/common-lisp/source/pictures/examples/road-demo\")
 (pictures::road-demo)
 press control-a to animate

"))


