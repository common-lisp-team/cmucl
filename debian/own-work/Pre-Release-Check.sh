#!/bin/bash

set -e

cmucl -batch -noinit -eval '(unix:unix-exit 0)'
cmucl -batch -noinit -eval '(require :gray-streams) (unix:unix-exit 0)'
cmucl -batch -noinit -eval '(require :clx) (unix:unix-exit 0)'
cmucl -batch -noinit -eval '(require :clm) (unix:unix-exit 0)'
cmucl -batch -noinit -eval '(require :hemlock) (unix:unix-exit 0)'
