(in-package :user)

(format t "THIS A A HUDGE SECURITY RISC. CHANGE THE PASSWORD!!!~%~%")
(setf mp::*idle-process* mp::*initial-process*)
(mp::start-lisp-connection-listener :port 6789 :password "Clara")

;;; now you can telnet in and do:
#|
$telnet localhost 6789
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
Enter password: "Clara"

CMU Common Lisp Experimental 18a+ release x86-linux 2.2.0 cvs, running on slartibartfast
Send bug reports and questions to your local CMU CL maintainer,
or to pw@snoopy.mv.com,
or to s950045@uia.ac.be or Peter.VanEynde@uia.ua.ac.be
or to cmucl-help@cons.org. (prefered)

type (help) for help, (quit) to exit, and (demo) to see the demos

Loaded subsystems:
    Python 1.0, target Intel x86
    CLOS based on PCL version:  September 16 92 PCL (f)
* (+ 1 1)

2
* (quit)
Connection closed by foreign host.
|#

