;;; -*- Mode: lisp -*-
;;
;; Graystream system definition
(in-package :asdf)

#+cmu
(defclass cl-modifying-file (cl-source-file) ())

#+cmu
(defmethod perform ((op load-op) (c cl-modifying-file))
  (ext:without-package-locks
     (call-next-method)))

#+cmu
(defmethod source-file-type ((c cl-modifying-file) (s module))
  "lisp")
  
#+cmu
(defsystem cmucl-graystream
    :components
  ((:file "gray-streams-class")
   (:cl-modifying-file  "gray-streams"
          :depends-on ("gray-streams-class"))))



