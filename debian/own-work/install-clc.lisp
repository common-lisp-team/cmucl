;;; -*- Mode: LISP; Package: CL-USER -*-
;;;
;;; Copyright Peter Van Eynde, 2001
;;;
;;; License: LGPL v2
;;;
(in-package "COMMON-LISP-USER")

;;; set up a reader macro to allow she-bang lines
(set-dispatch-macro-character #\# #\!
  (lambda (stream bang arg)
    (declare (ignore bang arg))
    (read-line stream)
    (values)))

(unless (ignore-errors
          (load "/usr/share/common-lisp/source/common-lisp-controller/common-lisp-controller.lisp"))
  (unix:unix-exit 1))

(unless (ignore-errors
          (common-lisp-controller:init-common-lisp-controller-v4 "cmucl")
          t)
  (format t "~%Error during init of common-lisp-controller~%")
  (unix:unix-exit 1))

(unless (ignore-errors
          ;; it loaded, configure it for common-lisp-controller use:
          (format t "~%Saving to new-lisp.core...")
          (ext:gc :full t)       
          (setf ext:*batch-mode* nil)
          (ext:save-lisp "new-lisp.core" 
                         :purify t))
  (unix:unix-exit 1))

