(in-package :user)

(defvar *max* nil)
(defun f ()
(let ((max nil))
  (format t "searching...")
  (x86::map-allocated-objects
     #'(lambda (obj type size) 
         (push (list size type obj)
               max))
   :static)
  (format t "~%")
  (setf max (sort max #'> :key #'first))
  (format t "found: ~D~%" (length max))
  (format t "max: ~D~%" 
    (loop for (size) in max
          maximize size))
  (format t "min: ~D~%" 
    (loop for (size) in max
          minimize size))
  (format t "avg: ~S~%" 
    (/ (loop for (size) in max
          sum size) (length max)))
  (loop for  (size type obj) in max 
        for n from 0 upto 10 do
   (format t "~%~D ~D " size type)
   (describe obj)
   (dolist (package (list-all-packages))
     (loop for x being each present-symbol of package 
           when (eq x obj) do
           (format t " ~S " x))))
  (values)))
